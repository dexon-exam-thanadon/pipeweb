import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { PipeRoutingModule } from './pipe-routing.module';
import { PipeComponent } from './pipe.component';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';

import { HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CmlComponent } from './cml/cml.component';
import { TestPointComponent } from './cml/test-point/test-point.component';
import { ThicknessComponent } from './cml/test-point/thickness/thickness.component';
import { MessageService } from 'primeng/api';


@NgModule({
  declarations: [PipeComponent, CmlComponent, TestPointComponent, ThicknessComponent],
  imports: [
    CommonModule,
    PipeRoutingModule,
    TableModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    CalendarModule,
    ToastModule
  ],
  providers: [HttpClient, DatePipe, MessageService]
})
export class PipeModule { }
