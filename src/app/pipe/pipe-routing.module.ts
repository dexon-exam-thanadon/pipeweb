import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PipeComponent } from './pipe.component';
import { CmlComponent } from './cml/cml.component';

const routes: Routes = [
  {
    path:'',
    component: PipeComponent
  },
  {
    path:'cml/:id',
    component: CmlComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PipeRoutingModule { }
