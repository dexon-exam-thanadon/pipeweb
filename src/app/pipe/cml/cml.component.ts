import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CmlService } from '../../service/cml.service';
import { ActivatedRoute } from '@angular/router';
import { Cml } from '../../inteface/cml';
import { InfoService } from '../../service/info.service';
import { Info } from '../../inteface/info';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-cml',
  templateUrl: './cml.component.html',
  styleUrl: './cml.component.scss'
})
export class CmlComponent implements OnInit {
  line_number: any
  cmls: any = []
  info: Info| undefined
  selectedCml: Cml ={
    line_number: '',
    cml_number: 0,
    cml_description: '',
    actual_outside_diameter: 0,
    design_thickness: 0,
    structural_thickness: 0,
    required_thickness: 0
  }
  isShow: boolean = false
  isAdd: boolean = false
  isEdit: boolean = false
  isDelete: boolean = false
  showTP: boolean = false

  constructor(public infoService: InfoService, 
    private cmlService: CmlService, 
    private route: ActivatedRoute,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.line_number = this.route.snapshot.paramMap.get('id')
    this.initCml()
  }

  initCml() {
    this.infoService.getInfo(this.line_number).then(value => {
      this.info = JSON.parse(value as string)[0]
    })

    this.cmlService.getCmls(this.line_number).then(value => {
      this.cmls = JSON.parse(value as string)
    })
  }

  add() {
    this.isShow = true
    this.isAdd = true

    let aod = this.cmlService.calcAOD(this.info!.pipe_size)
    let dt = this.cmlService.calcDT(this.info!, aod)
    let st = this.cmlService.calcST(this.info!.pipe_size)
    let rt = dt > st ? dt : st

    let cmlNumber = 1
    if(this.cmls.length > 0) {
      cmlNumber = this.cmls[this.cmls.length - 1].cml_number + 1
    }

    this.selectedCml = {
      line_number: this.line_number,
      cml_number: cmlNumber,
      cml_description: '',
      actual_outside_diameter: aod,
      design_thickness: dt,
      structural_thickness: st,
      required_thickness: rt
    }
  }

  showTestPoint(cml: Cml) {
    this.selectedCml = cml
    this.showTP = true
  }

  edit(cml: Cml) {
    this.isShow = true
    this.isEdit = true
    this.selectedCml = cml
  }

  delete(cml: Cml) {
    this.isDelete = true
    this.selectedCml = cml
  }

  close() {
    this.isShow = false
    this.isDelete = false
    this.isAdd = false
    this.isEdit = false
  }

  save(cml: Cml) {
    if(this.isAdd) {
      this.cmlService.addCml(cml).then(value => {
        this.initCml()
        this.showToast(value)
      })
    }

    if(this.isEdit) {
      this.cmlService.editCml(cml).then(value => {
        this.initCml()
        this.showToast(value)
      })
    }

    if(this.isDelete) {
      this.cmlService.deleteCml(cml).then(value => {
        this.initCml()
        this.showToast(value)
      })
    }
  }

  showToast(status: any) {
    this.messageService.clear()
    if(parseInt(status) == 200) {
      this.messageService.add({ severity: 'success', summary: 'Success'})
      this.close()
      this.showTP = false
    } 
    else {
      this.messageService.add({ severity: 'error', summary: 'Failed'})
    }
  }
}
