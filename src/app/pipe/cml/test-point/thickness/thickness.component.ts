import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Thickness } from '../../../../inteface/thickness';
import { ThicknessService } from '../../../../service/thickness.service';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-thickness',
  templateUrl: './thickness.component.html',
  styleUrl: './thickness.component.scss'
})
export class ThicknessComponent implements OnInit, OnChanges {
  @Input() cml_number: number = 0;
  @Input() tp_number: number = 0;

  line_number: any
  thicknesses: any = []
  selectedThickness: Thickness = {
    line_number: '',
    cml_number: 0,
    tp_number: 0,
    inspection_date: '',
    actual_thickness: 0
  }
  isShow: boolean = false
  isAdd: boolean = false
  isEdit: boolean = false
  isDelete: boolean = false

  constructor(private thicknessService: ThicknessService,
    private route: ActivatedRoute,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.line_number = this.route.snapshot.paramMap.get('id')
    this.initTestPoint()
  }

  ngOnChanges(): void {
    this.initTestPoint()
  }

  initTestPoint() {
    this.thicknessService.getThicknesses(this.line_number, this.cml_number, this.tp_number).then(value => {
      this.thicknesses = JSON.parse(value as string) || []
    })
  }

  formatDate(event: any) {
    let date = new Date(Date.parse(event));
    this.selectedThickness.inspection_date = `${date.getFullYear()}-${date.getMonth() < 9 ? 0 : ''}${date.getMonth() + 1}-${date.getDay() < 10 ? 0 : ''}${date.getDay()}`;
  }

  add() {
    this.isShow = true
    this.isAdd = true

    let date = new Date()

    this.selectedThickness = {
      line_number: this.line_number,
      cml_number: this.cml_number,
      tp_number: this.tp_number,
      inspection_date: `${date.getFullYear()}-${date.getMonth() < 9 ? 0 : ''}${date.getMonth() + 1}-${date.getDay() < 10 ? 0 : ''}${date.getDay()}`,
      actual_thickness: 0
    }
  }

  edit(tk: Thickness) {
    this.isShow = true
    this.isEdit = true
    this.selectedThickness = tk
  }

  delete(tk: Thickness) {
    this.isDelete = true
    this.selectedThickness = tk
  }

  close() {
    this.isShow = false
    this.isDelete = false
    this.isAdd = false
    this.isEdit = false
  }

  save(tk: Thickness) {
    if (this.isAdd) {
      this.thicknessService.addThickness(tk).then(value => {
        this.initTestPoint()
        this.showToast(value)
      })
    }

    if (this.isEdit) {
      this.thicknessService.editThickness(tk).then(value => {
        this.initTestPoint()
        this.showToast(value)
      })
    }

    if (this.isDelete) {
      this.thicknessService.deleteThickness(tk).then(value => {
        this.initTestPoint()
        this.showToast(value)
      })
    }
  }

  showToast(status: any) {
    this.messageService.clear()
    if (parseInt(status) == 200) {
      this.messageService.add({ severity: 'success', summary: 'Success' })
      this.close()
    }
    else {
      this.messageService.add({ severity: 'error', summary: 'Failed' })
    }
  }
}
