import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestPointComponent } from './test-point.component';

describe('TestPointComponent', () => {
  let component: TestPointComponent;
  let fixture: ComponentFixture<TestPointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestPointComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
