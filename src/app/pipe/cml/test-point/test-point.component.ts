import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TestPointService } from '../../../service/test-point.service';
import { TestPoint } from '../../../inteface/test-point';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-test-point',
  templateUrl: './test-point.component.html',
  styleUrl: './test-point.component.scss'
})
export class TestPointComponent implements OnInit, OnChanges {
  @Input() cml_number: number = 0;

  line_number: any
  testPoints: any = []
  selectedTestPoint: TestPoint = {
    line_number: '',
    cml_number: 0,
    tp_number: 0,
    tp_description: 0,
    note: ''
  }
  isShow: boolean = false
  isAdd: boolean = false
  isEdit: boolean = false
  isDelete: boolean = false
  showTK: boolean = false

  constructor(private testPointService: TestPointService,
    private route: ActivatedRoute,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.line_number = this.route.snapshot.paramMap.get('id')
    this.initTestPoint()
  }

  ngOnChanges(): void {
    this.initTestPoint()
    this.showTK = false
  }

  initTestPoint() {
    this.testPointService.getTestPoints(this.route.snapshot.paramMap.get('id')!, this.cml_number).then(value => {
      this.testPoints = JSON.parse(value as string) || []
    })
  }

  add() {
    this.isShow = true
    this.isAdd = true

    let tpNumber = 1
    if (this.testPoints.length > 0) {
      tpNumber = this.testPoints[this.testPoints.length - 1].tp_number + 1
    }

    this.selectedTestPoint = {
      line_number: this.line_number,
      cml_number: this.cml_number,
      tp_number: tpNumber,
      tp_description: 0,
      note: ''
    }
  }

  showThickness(tp: TestPoint) {
    this.selectedTestPoint = tp
    this.showTK = true
  }

  edit(tp: TestPoint) {
    this.isShow = true
    this.isEdit = true
    this.selectedTestPoint = tp
  }

  delete(tp: TestPoint) {
    this.isDelete = true
    this.selectedTestPoint = tp
  }

  close() {
    this.isShow = false
    this.isDelete = false
    this.isAdd = false
    this.isEdit = false
  }

  save(tp: TestPoint) {
    if (this.isAdd) {
      this.testPointService.addTestPoint(tp).then(value => {
        this.initTestPoint()
        this.showToast(value)
      })
    }

    if (this.isEdit) {
      this.testPointService.editTestPoint(tp).then(value => {
        this.initTestPoint()
        this.showToast(value)
      })
    }

    if (this.isDelete) {
      this.testPointService.deleteTestPoint(tp).then(value => {
        this.initTestPoint()
        this.showToast(value)
      })
    }
  }

  showToast(status: any) {
    this.messageService.clear()
    if (parseInt(status) == 200) {
      this.messageService.add({ severity: 'success', summary: 'Success' })
      this.close()
      this.showTK = false
    }
    else {
      this.messageService.add({ severity: 'error', summary: 'Failed' })
    }
  }
}
