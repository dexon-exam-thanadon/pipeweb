import { Component, OnInit } from '@angular/core';
import { InfoService } from '../service/info.service';
import { Info } from '../inteface/info';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrl: './pipe.component.scss'
})
export class PipeComponent implements OnInit {
  infos: any = []
  selectedInfo: Info = {
    line_number: '',
    location: '',
    from: '',
    to: '',
    drawing_number: '',
    service: '',
    material: '',
    inservice_date: '',
    pipe_size: 0,
    original_thickness: 0,
    stress: 0,
    joint_efficiency: 0,
    ca: 0,
    design_life: 0,
    design_pressure: 0,
    operating_pressure: 0,
    design_temperature: 0,
    operating_temperature: 0
  }
  dialogVisible: boolean = false
  isAdd: boolean = false
  isEdit: boolean = false
  isDelete: boolean = false
  isShow: boolean = false

  constructor(private infoService: InfoService, private router: Router, private messageService: MessageService) { }

  ngOnInit(): void {
    this.initInfo()
  }

  initInfo() {
    this.infoService.getInfos().then(value => {
      this.infos = value
    })
  }

  formatDate(event: any) {
    let date = new Date(Date.parse(event));
    this.selectedInfo.inservice_date = `${date.getFullYear()}-${date.getMonth() < 9 ? 0 : ''}${date.getMonth() + 1}-${date.getDay() < 10 ? 0 : ''}${date.getDay()}`;
  }

  show(info: any) {
    this.selectedInfo = info
    this.dialogVisible = true
    this.isShow = true
  }

  add() {
    let date = new Date()
    this.selectedInfo = {
      line_number: '',
      location: '',
      from: '',
      to: '',
      drawing_number: '',
      service: '',
      material: '',
      inservice_date: `${date.getFullYear()}-${date.getMonth() < 9 ? 0 : ''}${date.getMonth() + 1}-${date.getDay() < 10 ? 0 : ''}${date.getDay()}`,
      pipe_size: 0,
      original_thickness: 0,
      stress: 0,
      joint_efficiency: 0,
      ca: 0,
      design_life: 0,
      design_pressure: 0,
      operating_pressure: 0,
      design_temperature: 0,
      operating_temperature: 0
    }
    this.dialogVisible = true
    this.isAdd = true
  }

  edit() {
    this.isEdit = true
  }

  delete(info: Info) {
    this.isDelete = true
    this.selectedInfo = info
  }

  close() {
    this.dialogVisible = false
    this.isShow = false
    this.isEdit = false
    this.isAdd = false
    this.isDelete = false
  }

  save(info: Info) {
    if (this.isAdd) {
      this.infoService.addInfo(info).then((value:any) => {
        this.initInfo()
        this.showToast(value)
      })
    }

    if (this.isEdit) {
      this.infoService.editInfo(info).then((value:any) => {
        this.initInfo()
        this.showToast(value)
      })
    }

    if (this.isDelete) {
      this.infoService.deleteInfo(info).then((value:any) => {
        this.initInfo()
        this.showToast(value)
      })
    }
  }

  goToDetail(info: Info) {
    this.router.navigate([`cml/${info.line_number}`])
  }

  showToast(status: any) {
    this.messageService.clear()
    if(parseInt(status) == 200) {
      this.messageService.add({ severity: 'success', summary: 'Success'})
      this.close()
    } 
    else {
      this.messageService.add({ severity: 'error', summary: 'Failed'})
    }
  }
}
