import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Thickness } from '../inteface/thickness';

@Injectable({
  providedIn: 'root'
})
export class ThicknessService {
  private url: string = 'https://localhost:7207/api/thickness'

  constructor(private http: HttpClient) { }

  getThicknesses(lineNumber: string, cmlNumber: number, tpNumber: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url + '/getthicknessbytp', { line_number: lineNumber, cml_number: cmlNumber, tp_number: tpNumber }
        , httpOptions).subscribe(value => {
        resovle(value)
      })
    })
  }

  addThickness(tk: Thickness) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url, JSON.stringify(tk), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  editThickness(tk: Thickness) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.patch(this.url, JSON.stringify(tk), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  deleteThickness(tk: Thickness) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
      body: JSON.stringify(tk)
    }

    return new Promise((resovle, reject) => {
      this.http.delete(this.url, httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }
}
