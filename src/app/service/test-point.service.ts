import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TestPoint } from '../inteface/test-point';

@Injectable({
  providedIn: 'root'
})
export class TestPointService {
  private url: string = 'https://localhost:7207/api/testpoint'

  constructor(private http: HttpClient) { }

  getTestPoints(lineNumber: string, cmlNumber: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url + '/gettestpointbycml', { line_number: lineNumber, cml_number: cmlNumber }, httpOptions).subscribe(value => {
        resovle(value)
      })
    })
  }

  addTestPoint(tp: TestPoint) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url, JSON.stringify(tp), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  editTestPoint(tp: TestPoint) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.patch(this.url, JSON.stringify(tp), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  deleteTestPoint(tp: TestPoint) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
      body: JSON.stringify(tp)
    }

    return new Promise((resovle, reject) => {
      this.http.delete(this.url, httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }
}
