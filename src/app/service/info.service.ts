import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Info } from '../inteface/info';

@Injectable({
  providedIn: 'root'
})
export class InfoService {
  private url: string = 'https://localhost:7207/api/info'

  constructor(private http: HttpClient) { }

  getInfos() {
    return new Promise((resovle, reject) => {
      this.http.get(this.url).subscribe(value => {
        resovle(value)
      })
    })
  }

  getInfo(lineNumber: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url + '/getinfobylinenumber', { line_number: lineNumber }, httpOptions).subscribe(value => {
        resovle(value)
      })
    })
  }

  addInfo(info: Info) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url, JSON.stringify(info), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  editInfo(info: Info) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.patch(this.url, JSON.stringify(info), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  deleteInfo(info: Info) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
      body: JSON.stringify(info)
    }

    return new Promise((resovle, reject) => {
      this.http.delete(this.url, httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }
}
