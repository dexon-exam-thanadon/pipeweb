import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cml } from '../inteface/cml';
import { Info } from '../inteface/info';

@Injectable({
  providedIn: 'root'
})
export class CmlService {
  private url: string = 'https://localhost:7207/api/cml'

  constructor(private http: HttpClient) { }

  getCmls(lineNumber: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url + '/getcmlbyinfo', { line_number: lineNumber }, httpOptions).subscribe(value => {
        resovle(value)
      })
    })
  }

  addCml(cml: Cml) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.post(this.url, JSON.stringify(cml), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  editCml(cml: Cml) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json'
    }

    return new Promise((resovle, reject) => {
      this.http.patch(this.url, JSON.stringify(cml), httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  deleteCml(cml: Cml) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': '*/*',
        'Content-Type': 'application/json'
      }),
      responseType: 'text' as 'json',
      body: JSON.stringify(cml)
    }

    return new Promise((resovle, reject) => {
      this.http.delete(this.url, httpOptions).subscribe(value => {
        resovle(value)
      }, e => {
        resovle(e.status)
      })
    })
  }

  calcAOD(pipeSize: number) {
    let aod = 0
    switch (pipeSize) {
      case 0.125:
        aod = 10.3
        break;
      case 0.25:
        aod = 13.7
        break;
      case 0.357:
        aod = 17.1
        break;
      case 0.5:
        aod = 21.3
        break;
      case 0.75:
        aod = 26.7
        break;
      case 1:
        aod = 33.4
        break;
      case 1.25:
        aod = 42.2
        break;
      case 1.5:
        aod = 48.3
        break;
      case 2:
        aod = 60.3
        break;
      case 2.5:
        aod = 73
        break;
      case 3:
        aod = 88.9
        break;
      case 3.5:
        aod = 101.6
        break;
      case 4:
        aod = 114.3
        break;
      case 5:
        aod = 141.3
        break;
      case 6:
        aod = 168.3
        break;
      case 8:
        aod = 219.1
        break;
      case 10:
        aod = 273
        break;
      case 12:
        aod = 323.8
        break;
      case 14:
        aod = 355.6
        break;
      case 16:
        aod = 406.4
        break;
      case 18:
        aod = 457
        break;
      default:
        aod = 0
        break;
    }
    return aod
  }

  calcST(pipeSize: number) {
    let st = 0
    if (pipeSize <= 2) {
      st = 1.8
    }
    else if (pipeSize == 3) {
      st = 2
    }
    else if (pipeSize == 4) {
      st = 2.3
    }
    else if (pipeSize >= 6 && pipeSize <= 18) {
      st = 2.8
    }
    else if (pipeSize > 20) {
      st = 3.1
    }
    return st
  }

  calcDT(info: Info, aod: number) {
    let dt = info.design_pressure * aod / ((2 * info.stress * info.joint_efficiency) + (2 * info.design_pressure * 0.4))
    let formatDT = dt.toFixed(3)
    return parseFloat(formatDT)
  }
}
