export interface Cml {
    line_number: string
    cml_number: number,
    cml_description: string,
    actual_outside_diameter: number ,
    design_thickness: number,
    structural_thickness: number,
    required_thickness: number,
}