export interface TestPoint {
    line_number: string
    cml_number: number,
    tp_number: number,
    tp_description: number,
    note: string 
}