export interface Thickness {
    line_number: string
    cml_number: number,
    tp_number: number,
    inspection_date: string,
    actual_thickness: number 
}